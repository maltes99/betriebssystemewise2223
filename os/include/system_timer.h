#include <stdint.h>

#define ST 0xFFFFFD00

struct ST_
{
	unsigned int cr;
	unsigned int pimr;
	unsigned int wdmr;
    unsigned int rtmr;
	unsigned int sr;
	unsigned int ier;
	unsigned int idr;
	unsigned int imr;
	unsigned int rtar;
	unsigned int crtr;
};

#define PITS (1<<0)


void system_timer_init(void);
void set_pimr(unsigned int x);
unsigned int read_pimr(void);
void enable_pit_interrupt(void);
void disable_pit_interrupt(void);
void check_st_interrupt(void);
void sleep(unsigned int ms);
void set_rtmr(unsigned int x);
unsigned int read_crtr(void);
uint8_t pit_status(void);