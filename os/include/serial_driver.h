#include <stdarg.h>
#include <string.h>
#include <stdint.h>


#define DBGU 0xFFFFF200
#define TXRDY (1 << 1)
#define RXRDY (1 << 0)

// -------- (Offset: 0x0)  Control Register -------- 
#define DBGU_CR_RSTRX        (0x1 <<  2) // (USART) Reset Receiver
#define DBGU_CR_RSTTX        (0x1 <<  3) // (USART) Reset Transmitter
#define DBGU_CR_RXEN         (0x1 <<  4) // (USART) Receiver Enable
#define DBGU_CR_RXDIS        (0x1 <<  5) // (USART) Receiver Disable
#define DBGU_CR_TXEN         (0x1 <<  6) // (USART) Transmitter Enable
#define DBGU_CR_TXDIS        (0x1 <<  7) // (USART) Transmitter Disable
#define DBGU_CR_RSTSTA       (0x1 <<  8) // (USART) Reset Status Bits

// -------- (Offset: 0x14) Status Register -------- 
#define DBGU_SR_RXRDY        (0x1 <<  0) // (USART) Receiver Ready
#define DBGU_SR_TXRDY        (0x1 <<  1) // (USART) Transmitter Ready
#define DBGU_SR_ENDRX        (0x1 <<  3) // (USART) End of Receiver Transfer
#define DBGU_SR_ENDTX        (0x1 <<  4) // (USART) End of Transmitter Transfer
#define DBGU_SR_OVRE         (0x1 <<  5) // (USART) Overrun Error
#define DBGU_SR_FRAME        (0x1 <<  6) // (USART) Framing Error
#define DBGU_SR_PARE         (0x1 <<  7) // (USART) Parity Error
#define DBGU_SR_TXEMPTY      (0x1 <<  9) // (USART) Transmitter Empty
#define DBGU_SR_TXBUFE       (0x1 << 11) // (USART) Transmission Buffer Empty
#define DBGU_SR_RXBUFF       (0x1 << 12) // (USART) Receive Buffer Full


struct DBGU_{ 
	unsigned int cr; // DBGU_CR 0x0000 
	unsigned int mr; // DBGU_MR 0x0004 
	unsigned int ier; // DBGU_IER 0x0008 
	unsigned int idr; // DBGU_IDR 0x000C 
	unsigned int imr; // DBGU_IMR 0x0010
	unsigned int sr; // DBGU_SR 0x0014 
	unsigned int rhr; // DBGU_RHR 0x0018 
	unsigned int thr; // DBGU_THR 0x001C 
	unsigned int brgr; // DBGU_BRGR 0x0020
};

void print_(char *string);
void serial_tx(unsigned char c);
char serial_rx();
char* read_input();
void print_f(char *fmt, ...);
void print_int_to_hex(unsigned int value);
void print_ptr(uint32_t ptr);
char get_char(void);
void check_dbgu_interrupt(void);

