
/* Basis-Adresse für den Advanced Interrupt Controller */
#define AIC 0xFFFFF000

struct AIC_ {
    unsigned int smr[32]; // Source Mode Register 0-31
    unsigned int svr[32]; // Source Vector Register 0-31
    unsigned int ivr; // Interrupt Vector Register
    unsigned int fvr; // Fast Interrupt Vector Register
    unsigned int isr; // Interrupt Status Register
    unsigned int ipr; // Interrupt Pending Register
    unsigned int imr; // Interrupt Mask Register
    unsigned int cisr; // Core Interrupt Status Register
    unsigned int gap1[2]; // reserved address space 1;
    unsigned int iecr; // Interrupt Enable Command Register
    unsigned int idcr; // Interrupt Disable Command Register
    unsigned int iccr; // Interrupt Clear Command Register
    unsigned int iscr; // Interrupt Set Command Register
    unsigned int eoicr; // End of Interrupt Command Register
    unsigned int spu; // Spurious Interrupt Vector Register
    unsigned int dcr; // Debug Control Register
    unsigned int gap2; // reserved address space 2
};

/* Interrupt Quellen aktivieren und deaktivieren */
#define AIC_IECR 0x120
#define BIT_FIQ (1<<0)
#define BIT_IRQ (1<<1)
#define AIC_IDCR 0x124


void interrupts_init(void);

void disable_irq(void);
void disable_fiq(void);
void enable_irq(void);
void enable_fiq(void);

void source_0_handler(void);
void source_1_handler(void);
void source_random_handler(void);