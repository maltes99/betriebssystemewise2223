/* Prozessormode-Bits für das CPSR */
#define MODE_USR 0b10000
#define MODE_FIQ 0b10001
#define MODE_IRQ 0b10010
#define MODE_SVC 0b10011
#define MODE_ABT 0b10111
#define MODE_UND 0b11011
#define MODE_SYS 0b11111

/* Adressen der Stacks für die Prozessormodi. Jeder Stack ist 2KB groß. */
#define STACK_ADDR_FIQ 0x00204000
#define STACK_ADDR_IRQ 0x00203800
#define STACK_ADDR_SVC 0x00203000
#define STACK_ADDR_ABT 0x00202800
#define STACK_ADDR_UND 0x00202000
#define STACK_ADDR_USR 0x00201800



