#include "../include/cpu.h"

/* Initialisiert die Stacks */
.global stacks_init
stacks_init:
	mov r3, lr

	mov r2, pc
	mrs r0, cpsr
	
	/* FIQ Modus */
	bic r0, #31
	orr r0, #MODE_FIQ
	msr cpsr_c, r0
	ldr sp, =STACK_ADDR_FIQ
	
	/* IRQ Modus */
	bic r0, #31
	orr r0, #MODE_IRQ
	msr cpsr_c, r0
	ldr sp, =STACK_ADDR_IRQ
	
	/* Undefined Modus */
	bic r0, #31
	orr r0, #MODE_UND
	msr cpsr_c, r0
	ldr sp, =STACK_ADDR_UND
	
	/* Abort Modus */
	bic r0, #31
	orr r0, #MODE_ABT
	msr cpsr_c, r0
	ldr sp, =STACK_ADDR_ABT

	/* Supervisor Modus */
	bic r0, #31
	orr r0, #MODE_SVC
	msr cpsr_c, r0
	ldr sp, =STACK_ADDR_SVC

	/* System/User Modus */
	bic r0, #31
	orr r0, #MODE_SYS
	msr cpsr_c, r0
	ldr sp, =STACK_ADDR_USR
	
	bx r3

.global get_cpsr
get_cpsr:
	mrs r0, cpsr
	mov pc, lr