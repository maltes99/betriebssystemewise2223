#include "../include/interrupts.h"
#include "../include/serial_driver.h"
#include "../include/system_timer.h"
#include <stdint.h>


static volatile struct AIC_ *const aic = (struct AIC_ *) AIC;

void interrupts_init() {
    
    aic->svr[0] = (volatile uint32_t)&source_0_handler;
    aic->smr[0] = (0 << 5) | (7 << 0);

    aic->svr[1] = (volatile uint32_t)&source_1_handler;
    aic->smr[1] = (0 << 5) | (7 << 0);

    int i;
    for (i = 2; i < 5; i++) {
        aic->smr[i] = (0 << 5) | (0 << 0);
        aic->svr[i] = (volatile uint32_t)&source_random_handler;
    }

    /* Interrupts im ARM Status Register zulassen  */
    enable_irq();
    enable_fiq();

    /* Interrupts im Advanced Interrupt Controller aktivieren */
    aic->iecr = (1<<1);

    print_("Interrupts set.");
}

void enable_fiq() {
    __asm__ volatile(
        "mrs r2, cpsr\n\t"
        "bic r2, r2, #0x40\n\t" /* Das "F" Bit auf 0 setzen */
        "msr cpsr, r2\n\t"
    ); 
}

void enable_irq() {
    __asm__ volatile(
        "mrs r2, cpsr\n\t"
        "bic r2, r2, #0x80\n\t" /* Das "I" Bit auf 0 setzen */
        "msr cpsr, r2\n\t");
}

void disable_fiq() {
    __asm__ volatile (
        "mrs r2, cpsr\n\t"
        "orr r2, r2, #0x40\n\t" /* Das "F" Bit auf 1 setzen */
        "msr cpsr, r2\n\t");
}

void disable_irq() {
    __asm__ volatile(
        "mrs r2, cpsr\n\t"
        "orr r2, r2, #0x80\n\t" /* Das "I" Bit auf 1 setzen */
        "msr cpsr, r2\n\t");
}



void source_random_handler() {
    unsigned int tmp = aic->ivr;
    print_("---------- Random source on AIC interrupted. ---------");
    aic->eoicr = 1;
    enable_fiq();
}

void source_0_handler() {
    print_("---------- Source 0 on AIC interrupted. ---------");
    enable_fiq();
}

void source_1_handler(void) {
    check_st_interrupt();
    check_dbgu_interrupt();
    aic->eoicr = 1;
    enable_irq();
    print_("END ISR");
}