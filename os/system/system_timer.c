#include "../include/system_timer.h"
#include "../include/serial_driver.h"

// Intervall für Periodic Intervall Timer auf 1s setzen
#define SYSTEM_TIMER_PIMR (32768 / 2)
// Real Time Timer Genaugkeit auf 1ms setzen
#define SYSTEM_TIMER_RTMR (32768 / 1000)

static volatile struct ST_ * const st = (struct ST_ *)ST;

void system_timer_init() {
	print_("Set ST");
	set_pimr(SYSTEM_TIMER_PIMR);
    set_rtmr(SYSTEM_TIMER_RTMR);
	enable_pit_interrupt();
	print_("ST set.");
}

void enable_pit_interrupt() {
	st->ier = PITS;
}

void disable_pit_interrupt() {
	st->idr = PITS;
}

void set_pimr(unsigned int x) {
	st->pimr = (uint32_t) x;
}

unsigned int read_pimr() {
	return st->pimr;
}

uint8_t pit_status() {
	return st->sr & PITS;
}

void set_rtmr(unsigned int x) {
	st->rtmr = x;
}

unsigned int read_crtr() {
	return st->crtr;
}

void check_st_interrupt() {
	if (pit_status()) {
		print_("!");
	}
}

void sleep(unsigned int ms) {
    unsigned int start = read_crtr();
    while (read_crtr()-start < ms);
    return;
}