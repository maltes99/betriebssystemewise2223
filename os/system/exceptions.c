#include <stdint.h>
#include "../include/exceptions.h"
#include "../include/serial_driver.h"
#include "../include/system_timer.h"

extern unsigned int ivt_to_trampoline;
extern unsigned int ivt_to_aic_ivr;
extern unsigned int ivt_to_aic_fiq;

extern void software_interrupt_trampoline(void);
extern void data_abort_trampoline(void);
extern void undefined_instruction_trampoline(void);
extern void prefetch_abort_trampoline(void);
extern void unhandled_trampoline(void);
extern void irq_trampoline(void);


void exceptions_init(void) {
	// Remapping memory
	volatile unsigned int *mc_rcr = (unsigned int *)(MC + MC_RCR);
	*mc_rcr = (1 << 0);
	
	volatile unsigned int *ptr;
	
	// Den relativen Sprungbefehl ivt_command schreiben wir an jede Stelle der IVT
	ptr = (unsigned int *)0x00; *ptr = ivt_to_trampoline; // Reset
	ptr = (unsigned int *)0x04; *ptr = ivt_to_trampoline; // Undefined Instruction
	ptr = (unsigned int *)0x08; *ptr = ivt_to_trampoline; // Software interrupt
	ptr = (unsigned int *)0x0C; *ptr = ivt_to_trampoline; // Prefetch abort
	ptr = (unsigned int *)0x10; *ptr = ivt_to_trampoline; // Data abort
	ptr = (unsigned int *)0x18; *ptr = ivt_to_aic_ivr; // Hardware Interrupt
	ptr = (unsigned int *)0x1C; *ptr = ivt_to_aic_fiq; // Fast Interrupt
	
	// Die Adressen der Trampoline schreiben wir an die Ziele der Sprungbefehle
	ptr = (unsigned int *)0x20; *ptr = (unsigned int)unhandled_trampoline;
	ptr = (unsigned int *)0x24; *ptr = (unsigned int)undefined_instruction_trampoline;
	ptr = (unsigned int *)0x28; *ptr = (unsigned int)software_interrupt_trampoline;
	ptr = (unsigned int *)0x2C; *ptr = (unsigned int)prefetch_abort_trampoline;
	ptr = (unsigned int *)0x30; *ptr = (unsigned int)data_abort_trampoline;

	print_("Exceptions set.");
}

void software_interrupt_handler(uint32_t addr, uint32_t instruction) {
	print_f("SoftwareInterruptException thrown at address: %x\n       In function %x\n", addr, instruction);
}

void prefetch_abort_handler(uint32_t addr, uint32_t instruction) {
	print_f("ERROR! PrefetchAbortException thrown at address: %x\n       In function %x\n", addr, instruction);
}

void data_abort_handler(uint32_t addr, uint32_t instruction) {
	print_f("ERROR! DataAbortException thrown at address: %x\n       In function %x\n", addr, instruction);
}

void undefined_instruction_handler(uint32_t addr, uint32_t instruction) {
	print_f("ERROR! UndefinedInstructionException thrown at address: %x\n       In function %x\n", addr, instruction);
}
