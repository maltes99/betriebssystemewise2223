#include "../include/serial_driver.h"
#include "../include/exceptions.h"

char input_buffer[256];
char last_input = '\n';
static const char HEX_DIGITS[] = "0123456789abcdef";

volatile struct DBGU_ * const dbgu = (struct DBGU_ *)DBGU;

void serial_init(void) {
	print_("Set DBGU.");
  	dbgu->ier = RXRDY;
	print_("DBGU set.");
}

void print_int_to_hex(unsigned int value) {
	char buffer[8];
	buffer[0] = '0';

	int i;
	for (i=0; value != 0; i++) {
		buffer[i] = HEX_DIGITS[value % 16];
		value /= 16;
	}

	serial_tx('0');
	serial_tx('x');
	while(i >= 0) {
		serial_tx(buffer[i]);
		i--;
	}
}

void print_int(unsigned int value) {
	char buffer[16];

	int i;
	for (i=0; value != 0; i++) {
		buffer[i] = HEX_DIGITS[value % 10];
		value /= 10;
	}

	while(i >= 0) {
		serial_tx(buffer[i]);
		i--;
	}
}

void print_(char *string) {
	int i;
	char c;
	for (i=0; (c = string[i] & 0xff) != 0; i++) { // 
		serial_tx(c);
	}
	serial_tx('\n');
}

void print_f(char *fmt, ...) {
	va_list args;
	int i;
	char c;

	va_start(args, fmt);
	for (i=0; (c = fmt[i] & 0xff) != 0; i++) { // 
		if (c != '%') {
			serial_tx(c);
			continue;
		}
		
		c = fmt[++i] & 0xff;
		if(c == 0) {
			break;
		}

		switch (c) {
    	case 'c':
			serial_tx(va_arg(args, int));
			break;
		case 's':
			print_(va_arg(args, char *));
			break;
		case 'x':
			print_int_to_hex(va_arg(args, unsigned int));
			break;
		case 'p':
			print_int_to_hex(va_arg(args, uint32_t)); // pointer as hex value
			break;
		case 'i':
			print_int(va_arg(args, int));
		default:
			break;
			// TypeUnkownError
		}

	}
	va_end(args);
}

char* read_input() {
	int i;
	char c = serial_rx();
	for (i=0; c != '\r' && i < 256; i++) { // 
		input_buffer[i] = c;
		c = serial_rx();
	}
	input_buffer[i] = '\0';
	serial_tx('\n');
	return input_buffer;
}

void serial_tx(unsigned char c) { 
	while (!(dbgu->sr & TXRDY));
	dbgu->thr = (int)c;
}

char serial_rx() {
	while (!(dbgu->sr & RXRDY));
	return dbgu->rhr;
}

char get_char() {
	return last_input;
}

void check_dbgu_interrupt() {
	char c = '0';

	if (dbgu->sr & RXRDY) {
		last_input = dbgu->rhr;
	}
}