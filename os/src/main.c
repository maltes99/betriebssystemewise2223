#include "../include/serial_driver.h"
#include "../include/exceptions.h"
#include "../include/system_timer.h"
#include <stdint.h>

void main (void) {

    /*               
    *   Assignment 1 - Serial interface
    */               
	// print_f("Hallo %carlo! %s und das sogar %x-fach :D an der Stelle %p", 'C', "Es macht Spaß :)", 45791, &dbgu->mr);
	// print_("\nHier können Sie sich mit Ihrer Tastatur austoben :)\n");
	/*Example DBGU output */
	// while(1) {
	// 	char c = serial_rx();
	// 	if (c == '\r') {
	// 		serial_tx('\n');
	// 	}else {
	// 		serial_tx(c);
	// 	}
	// }

    /*               
    *   Assignment 2 - Exception handling
    */               
    // int next = 1;
    // while(next) {
    //     print_("Wählen Sie einen Exception zum Auslösen:");
    //     print_("    1 - Data Abort Exception");
    //     print_("    2 - Undefined Instruction Exception");
    //     print_("    3 - Software Interrupt");
    //     print_("    0 - Abfrage beenden");
    //     switch (read_input()[0]) {
    //         case '1':
    //             print_("Trigger Data Abort Exception...");
    //             int *a = 0x00400001;
    //             *a = 5;
    //             break;
    //         case '2':
    //             print_("\nTrigger Undefined Excepion...");
    //             __asm__ volatile("cdp p3, 3, c10, c10, c3, 3 \n\t");
    //             break;
    //         case '3':
    //             print_("\nTrigger Software Interrupt...");
    //             __asm__ volatile("swi 1\n\t");
    //             break;
    //         case '0':
    //             print_("Exited loop.");
    //             next = 0;
    //             break;
    //         default:
    //             print_("Ungültige Eingabe");
    //             break;
    //     }
    //     print_f("Next: %x", next);
    //     print_("\n");
    // }

    /*               
    *   Assignment 3 - Hardware Interrupts
    */ 
    print_("Type a char to print out char 20x");
    while(1) {
        char c = get_char();
        int i;
        for (i = 0; i < 20; i++) {
            serial_tx(c);
            sleep(100);
        }
    }
}