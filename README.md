# BetriebssystemeWiSe2223



# Workflow
- Kompilieren mit `arm-none-eabi-gcc -Wall -Wextra -ffreestanding -mcpu=arm920t -O2 -c <.c datei>`
- Linken mit `arm-none-eabi-ld -Tkernel.lds -o kernel <objektdateien>`
- Kernel Datei auf Andorra kopieren mit `scp kernel <nutzer>@anrodda.imp.fu-berlin.de:<Ziel-Dateipfad>`
- Ausführen mit `/home/mi/linnert/arm/bin/qemu-system-arm -M portux920t -m 64M -nographic -kernel kernel` 
- Auf Andorra muus noch der Library_Path expotiert werden: `LD_LIBRARY_PATH=/usr/local/lib:/import/sage-7.4/local/lib/ && export LD_LIBRARY_PATH`
